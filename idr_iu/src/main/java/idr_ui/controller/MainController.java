package idr_ui.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;

import idr.core.DataManager;
import idr_ui.view.FileSelector;
import idr_ui.view.ViewComponent;

/**
 * Controlador para la ventana 
 * @author Ver�nica Campos
 *
 */
public class MainController extends Controller{

	public MainController(ViewComponent view, DataManager dataManager) {
		super(view,dataManager);
		dataManager.addListener(this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		// Falta indicar que hace en caso de un cambio del modelo
	}
	
	public ActionListener setbuttonListener(FileSelector fileSelector) {
		return new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e) {
        		fileSelector.selectFile();
        	}
		};
	}

}
