package idr_ui.controller;

import java.beans.PropertyChangeListener;

import idr.core.DataManager;
import idr_ui.view.ViewComponent;

public abstract class Controller implements PropertyChangeListener{
	protected ViewComponent view;
	protected DataManager dataManager;
	
	public Controller (ViewComponent view,DataManager dataManager) {
		this.view = view;
		this.dataManager = dataManager;
	}
	
	public Controller (ViewComponent view) {
		this.view = view;
	}
}
