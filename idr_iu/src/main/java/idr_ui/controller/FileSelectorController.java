package idr_ui.controller;

import java.beans.PropertyChangeEvent;
import java.io.File;

import idr.core.DataManager;
import idr_ui.view.ViewComponent;

public class FileSelectorController extends Controller{

	public FileSelectorController(ViewComponent view, DataManager dataManager) {
		super(view, dataManager);
		dataManager.addListener(this);
	}
	
	public void sendFile(File file) {
		dataManager.retrieveInvoiceData(file);
	}

	/**
	 * Si el modelo encuentra un retriever, lanza un aviso
	 * entrega el nombre del invoice ingresado 
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
	}

}
