package idr_ui.controller;

import java.beans.PropertyChangeEvent;

import idr.core.DataManager;
import idr_ui.view.InvoiceForm;

public class FormController extends Controller{
	
	public FormController(InvoiceForm view, DataManager dataManager) {
		super(view,dataManager);
		dataManager.addListener(this);
	}
	
	/**
	 * Espera la llegada del cambio de los datos.
	 * Una vez que se obtienen los datos, usa los datos 
	 * desde el modelo para actualizar la vista
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String eventName = evt.getPropertyName();
		if(eventName!=null && eventName.equals("invoiceReady")) {
			dataManager.sendCuitSender();
		}
	}

}
