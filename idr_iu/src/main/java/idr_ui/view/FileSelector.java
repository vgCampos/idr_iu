package idr_ui.view;

import java.beans.PropertyChangeEvent;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import idr.core.DataManager;
import idr_ui.controller.FileSelectorController;

@SuppressWarnings("serial")
public class FileSelector extends JFileChooser 
		implements ViewComponent{	
	
	private FileNameExtensionFilter filter;
	private FileSelectorController control;

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
	}
	
	public FileSelector(DataManager dataManager){
		control = new FileSelectorController(this, dataManager);
		this.setFileSelectionMode(JFileChooser.FILES_ONLY);
		filter = new FileNameExtensionFilter
				("*.pdf", "pdf", "*.PDF", "PDF","*.jpg", "jpg", "*.JPG", "JPG");
		this.setFileFilter(filter);
		this.addPropertyChangeListener(control);
	}
	
	public void selectFile() {
		int result = this.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION) {
			File file = this.getSelectedFile();
			control.sendFile(file);
        }
	}

}
