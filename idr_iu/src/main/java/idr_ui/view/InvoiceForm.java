package idr_ui.view;

import java.beans.PropertyChangeEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import idr.core.DataManager;
import idr_ui.controller.FormController;
import net.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class InvoiceForm extends JPanel implements ViewComponent{
	
	//private JTextField textCuitSender,textCuitReceiver;
	private JTextField textCuitSender;
	
	public InvoiceForm(DataManager manager) {
        new FormController(this,manager);
        manager.addListener(this);
		JLabel label = new JLabel("Resultados:");
		JLabel labelCuitEmisor = new JLabel("Cuit Emisor:");
		//JLabel labelCuitReceptor = new JLabel("Cuit Receptor:");
		textCuitSender = new JTextField(20);
	//	textCuitReceiver = new JTextField(20);
		textCuitSender.setEditable(false);
		//textCuitReceiver.setEditable(false);
        
        this.setLayout(new MigLayout());
        this.add(label,"wrap");
        this.add(labelCuitEmisor,"gap");
        this.add(textCuitSender,"wrap");
       // this.add(labelCuitReceptor,"gap");
        //this.add(textCuitReceiver,"wrap");
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String eventName = evt.getPropertyName();
		if(eventName.equals("cuitSender")) {
			String cuitSender = (String) evt.getNewValue();
			if(cuitSender==null) {
				textCuitSender.setText("No se encontr�");
			}
			textCuitSender.setText(cuitSender);
		}
		if(eventName.equals("supportInvoice")) {
			textCuitSender.setText("");
		}
	}
	

}
