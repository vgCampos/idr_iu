package idr_ui.view;

import java.beans.PropertyChangeEvent;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;
import ret.PdfDataRetriever;
import idr.core.DataManager;
import idr.core.InvoiceDataManager;
import idr.core.extraction.Retriever;
import idr_ui.controller.MainController;

@SuppressWarnings("serial")
public class MainView extends JFrame implements ViewComponent{

	private MainController control;
	
	public static void main(String[] args) {
		PdfDataRetriever retriever = new PdfDataRetriever();
		Set<Retriever> retrievers = new HashSet<Retriever>();
		retrievers.add(retriever);
		
		DataManager manager = new InvoiceDataManager(retrievers);
		new MainView(manager);
	}	
	/**
	 * Create the application.
	 */
	public MainView(DataManager manager){
		initialize(manager);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(DataManager manager) {
        this.control = new MainController(this,manager);
        this.setTitle("Invoice Data Retriever");
		JLabel label = new JLabel("Selecciona la factura:");
        JButton searchButton = new JButton("Buscar");
        FileSelector fileSelector = new FileSelector(manager);
        InvoiceForm form = new InvoiceForm(manager);
        
        searchButton.addActionListener(control.setbuttonListener(fileSelector));
        
        this.setLayout(new MigLayout());
        this.add(form,"dock south");
        this.add(label,"west");
        this.add(searchButton,"east");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.pack();
  		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String eventName = evt.getPropertyName();
		if(eventName!=null && eventName.equals("support")) {
			///String message = (String) evt.getNewValue();
		}
	}

}
